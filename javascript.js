function validateForm() {

	//declare questions variable
	var one = document.forms["theForm"]["one"].value;
	var two = document.forms["theForm"]["two"].value;
	var three = document.forms["theForm"]["three"].value;
	var four = document.forms["theForm"]["four"].value;
	var five = document.forms["theForm"]["five"].value;
	var six = document.forms["theForm"]["six"].value;
	var seven = document.forms["theForm"]["seven"].value;
	var eight = document.forms["theForm"]["eight"].value;
	var nine = document.forms["theForm"]["nine"].value;
	var ten = document.forms["theForm"]["ten"].value;

	//to check if answer is selected for each question
	if (one== "") {
		alert ("Oops! Question 1 is required");
		return false;
	}

	 else if (two== "") {
		alert ("Oops! Question 2 is required");
		return false;
	}

	if (three== "") {
		alert ("Oops! Question 3 is required");
		return false;
	}

	if (four== "") {
		alert ("Oops! Question 4 is required");
		return false;
	}

	if (five== "") {
		alert ("Oops! Question 5 is required");
		return false;
	}

	if (six== "") {
		alert ("Oops! Question 6 is required");
		return false;
	}

	if (seven== "") {
		alert ("Oops! Question 7 is required");
		return false;
	}

	if (eight== "") {
		alert ("Oops! Question 8 is required");
		return false;
	}

	if (nine== "") {
		alert ("Oops! Question 9 is required");
		return false;
	}

	if (ten== "") {
		alert ("Oops! Question 10 is required");
		return false;
	}

	//declare name variable 
	var name = document.forms["theForm"]["name"].value;

	//to count score
	var score=0;
	if (one == "Cascading Style Sheets") {
		score++;
	}

	if (two == "style") {
		score++;
	}

	if (three == "background-color") {
		score++;
	}

	if (four == "color") {
		score++;
	}

	if (five == "font-size") {
		score++;
	}

	if (six == "font-weight:bold;") {
		score++;
	}

	if (seven == "Separate each selector with a comma") {
		score++;
	}

	if (eight == "static") {
		score++;
	}

	if (nine == "#demo") {
		score++;
	}

	if (ten == "No") {
		score++;
	}

		
		

	//to display score
	if (score>=0 && score<=4) {
		alert("Keep trying, " + name + "! You answered "+ score +" out of 10 correctly");
	}

	else if (score>=5 && score<=9) {
		alert("Way to go, " + name + " ! You got "+ score +" out of 10 correct");
	}

	else if (score==10) {
		alert("Congratulations "+ name + " ! You got "+ score +" out of 10");
	}

}
